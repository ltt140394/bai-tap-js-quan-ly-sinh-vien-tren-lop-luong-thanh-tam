function layThongTinTuForm() {
    var maSv = document.getElementById("txtMaSV").value;
    var tenSv = document.getElementById("txtTenSV").value;
    var emailSv = document.getElementById("txtEmail").value;
    var diemToan = document.getElementById("txtDiemToan").value * 1;
    var diemLy = document.getElementById("txtDiemLy").value * 1;
    var diemHoa = document.getElementById("txtDiemHoa").value * 1;

    var sinhVien = new SinhVien(maSv, tenSv, emailSv, diemToan, diemLy, diemHoa);
    return sinhVien;
}

function xuatThongTinLenForm(sv) {
    document.getElementById("txtMaSV").value = sv.maSv;
    document.getElementById("txtTenSV").value = sv.tenSv;
    document.getElementById("txtEmail").value = sv.emailSv;
    document.getElementById("txtDiemToan").value = sv.diemToan;
    document.getElementById("txtDiemLy").value = sv.diemLy;
    document.getElementById("txtDiemHoa").value = sv.diemHoa;
}

function xuatDanhSachSinhVien(dssv) {
    var contentHTML = "";
    for (i = 0; i < dssv.length; i++) {
        var sinhVien = dssv[i];
        var contentTr = /*html */ `
        <tr>
        <td>${sinhVien.maSv}</td>
        <td>${sinhVien.tenSv}</td>
        <td>${sinhVien.emailSv}</td>
        <td>${sinhVien.tinhDTB()}</td>
        <td>
        <button class="btn btn-danger" onclick="xoaSinhVien('${sinhVien.maSv}')">Xóa</button>
        <button class="btn btn-success" onclick="suaSinhVien('${sinhVien.maSv}')">Sửa</button>
        </td>
        </tr>
        `;
        contentHTML += contentTr;
    }
    document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}

function resetInput() {
    document.getElementById("txtMaSV").value = "";
    document.getElementById("txtTenSV").value = "";
    document.getElementById("txtEmail").value = "";
    document.getElementById("txtDiemToan").value = "";
    document.getElementById("txtDiemLy").value = "";
    document.getElementById("txtDiemHoa").value = "";
}