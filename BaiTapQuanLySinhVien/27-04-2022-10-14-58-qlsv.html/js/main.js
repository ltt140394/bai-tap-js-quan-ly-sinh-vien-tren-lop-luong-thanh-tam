var danhSachSinhVien = [];

var validatorSV = new ValidatorSV();

const DSSV_LOCALSTORAGE = "DSSV_LOCALSTORAGE";

// Hàm convert array thành json để có thể lưu vào local storage
const luuLocalStorage = function () {
    var dssvJson = JSON.stringify(danhSachSinhVien);
    localStorage.setItem(DSSV_LOCALSTORAGE, dssvJson);
}

// lấy dữ liệu từ json gán lại cho array gốc và render lại giao diện khi load trang
var dssvJson = localStorage.getItem(DSSV_LOCALSTORAGE);
if (dssvJson) {
    danhSachSinhVien = JSON.parse(dssvJson);
    danhSachSinhVien = danhSachSinhVien.map(function (item) {
        return new SinhVien(item.maSv, item.tenSv, item.emailSv, item.diemToan, item.diemLy, item.diemHoa);
    });
    xuatDanhSachSinhVien(danhSachSinhVien);
}

const timKiemViTri = function (id, array) {
    return array.findIndex(function (item) {
        return item.maSv == id;
    });
}

function themSinhVien() {
    var newSinhVien = layThongTinTuForm();

    var isValidAll = true;

    var isValidMaSV = validatorSV.kiemTraRong("txtMaSV", "spanMaSV", "Mã sinh viên không được để trống") && validatorSV.kiemTraIDHopLe(newSinhVien, danhSachSinhVien);
    var isValidTenSV = validatorSV.kiemTraRong("txtTenSV", "spanTenSV", "Tên không được để trống");
    var isValidEmail = validatorSV.kiemTraRong("txtEmail", "spanEmailSV", "Email không được để trống") && validatorSV.kiemTraEmail("txtEmail", "spanEmailSV");
    var isValidDiemToan = validatorSV.kiemTraRong("txtDiemToan", "spanToan", "Điểm Toán không được để trống") && validatorSV.kiemTraDiemHopLe("txtDiemToan", "spanToan") && validatorSV.kiemTraThangDiem("txtDiemToan", "spanToan");
    var isValidDiemLy = validatorSV.kiemTraRong("txtDiemLy", "spanLy", "Điểm Lý không được để trống") && validatorSV.kiemTraDiemHopLe("txtDiemLy", "spanLy") && validatorSV.kiemTraThangDiem("txtDiemLy", "spanLy");
    var isValidDiemHoa = validatorSV.kiemTraRong("txtDiemHoa", "spanHoa", "Điểm Hóa không được để trống") && validatorSV.kiemTraDiemHopLe("txtDiemHoa", "spanHoa") && validatorSV.kiemTraThangDiem("txtDiemHoa", "spanHoa");

    isValidAll = isValidMaSV && isValidTenSV && isValidEmail && isValidDiemToan && isValidDiemLy && isValidDiemHoa;

    if (isValidAll) {
        danhSachSinhVien.push(newSinhVien);
        xuatDanhSachSinhVien(danhSachSinhVien);
        resetInput();
        luuLocalStorage();
    }
}

function xoaSinhVien(id) {
    var viTri = timKiemViTri(id, danhSachSinhVien);
    danhSachSinhVien.splice(viTri, 1);
    xuatDanhSachSinhVien(danhSachSinhVien);
    luuLocalStorage();
}

function suaSinhVien(id) {
    var viTri = timKiemViTri(id, danhSachSinhVien);
    // console.log({viTri});
    var sinhVienDuocSua = danhSachSinhVien[viTri];
    xuatThongTinLenForm(sinhVienDuocSua);
}

function capNhatSinhVien() {
    var sinhVienDuocSua = layThongTinTuForm();

    var isValidAll = true;

    var isValidMaSV = validatorSV.kiemTraRong("txtMaSV", "spanMaSV", "Mã sinh viên không được để trống");
    var isValidTenSV = validatorSV.kiemTraRong("txtTenSV", "spanTenSV", "Tên không được để trống");
    var isValidEmail = validatorSV.kiemTraRong("txtEmail", "spanEmailSV", "Email không được để trống") && validatorSV.kiemTraEmail("txtEmail", "spanEmailSV");
    var isValidDiemToan = validatorSV.kiemTraRong("txtDiemToan", "spanToan", "Điểm Toán không được để trống") && validatorSV.kiemTraDiemHopLe("txtDiemToan", "spanToan") && validatorSV.kiemTraThangDiem("txtDiemToan", "spanToan");
    var isValidDiemLy = validatorSV.kiemTraRong("txtDiemLy", "spanLy", "Điểm Lý không được để trống") && validatorSV.kiemTraDiemHopLe("txtDiemLy", "spanLy") && validatorSV.kiemTraThangDiem("txtDiemLy", "spanLy");
    var isValidDiemHoa = validatorSV.kiemTraRong("txtDiemHoa", "spanHoa", "Điểm Hóa không được để trống") && validatorSV.kiemTraDiemHopLe("txtDiemHoa", "spanHoa") && validatorSV.kiemTraThangDiem("txtDiemHoa", "spanHoa");

    isValidAll = isValidMaSV && isValidTenSV && isValidEmail && isValidDiemToan && isValidDiemLy && isValidDiemHoa;

    if (isValidAll) {
        var viTri = timKiemViTri(sinhVienDuocSua.maSv, danhSachSinhVien);
        danhSachSinhVien[viTri] = sinhVienDuocSua;
        xuatDanhSachSinhVien(danhSachSinhVien);
        resetInput();
        luuLocalStorage();
    }
}

function timSinhVien() {
    var danhSachSinhVienCanTim = [];
    var tenSinhVienCanTim = document.getElementById("txtSearch").value;
    danhSachSinhVien.forEach(function (item) {
        if (item.tenSv == tenSinhVienCanTim) {
            danhSachSinhVienCanTim.push(item);
        };

        xuatDanhSachSinhVien(danhSachSinhVienCanTim);
    });
}

function resetDanhSachSinhVien() {
    xuatDanhSachSinhVien(danhSachSinhVien);
}

function hienThiLaiDSSVKhiXoaTextSearch() {
    var txtSearch = document.getElementById("txtSearch").value;

    if (!txtSearch) {
        xuatDanhSachSinhVien(danhSachSinhVien);
    }
}