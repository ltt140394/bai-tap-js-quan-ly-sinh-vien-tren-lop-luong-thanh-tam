function ValidatorSV() {
    this.kiemTraRong = function (idTarget, idError, messageError) {
        var valueTarget = document.getElementById(idTarget).value.trim();
        if (!valueTarget) {
            document.getElementById(idError).innerText = messageError;
            return false;
        } else {
            document.getElementById(idError).innerText = "";
            return true;
        }
    }

    this.kiemTraIDHopLe = function (newSinhVien, danhSachSinhVien) {
        var index = danhSachSinhVien.findIndex(function (item) {
            return item.maSv == newSinhVien.maSv;
        });

        if (index == -1) {
            document.getElementById("spanMaSV").innerText = "";
            return true;
        } else {
            document.getElementById("spanMaSV").innerText = "Mã sinh viên không được trùng";
            return false;
        }
    }

    this.kiemTraEmail = function (idTarget, idError) {
        var valueTarget = document.getElementById(idTarget).value.trim();
        var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/

        if (regex.test(valueTarget)) {
            document.getElementById(idError).innerText = "";
            return true;
        } else {
            document.getElementById(idError).innerText = "Email không hợp lệ";
            return false;
        }
    }

    this.kiemTraDiemHopLe = function (idTarget, idError) {
        var valueTarget = document.getElementById(idTarget).value.trim();
        var regex = /^[0-9_-]+$/;

        if (valueTarget.match(regex)) {
            document.getElementById(idError).innerText = "";
            return true;
        } else {
            document.getElementById(idError).innerText = "Điểm không hợp lệ. Điểm phải là chữ số";
            return false;
        }
    }

    this.kiemTraThangDiem = function (idTarget, idError) {
        var valueTarget = document.getElementById(idTarget).value * 1;

        if (valueTarget >= 0 && valueTarget <= 10) {
            document.getElementById(idError).innerText = "";
            return true;
        } else {
            document.getElementById(idError).innerText = "Điểm không hợp lệ. Điểm phải trong thang điểm 0-10";
            return false;
        }
    }
}