function lamTronSoSauPhay (num,n) {
    //num : số cần xử lý
    //n: số chữ số sau dấu phẩy cần lấy
    var base = 10**n;
    var result = Math.round(num * base) / base ;
    return result;
}

var SinhVien = function(_ma,_ten,_email,_toan,_ly,_hoa) {
    this.maSv = _ma;
    this.tenSv = _ten;
    this.emailSv = _email;
    this.diemToan = _toan;
    this.diemLy = _ly;
    this.diemHoa = _hoa;
    this.tinhDTB = function() {
        return lamTronSoSauPhay(((this.diemToan + this.diemLy + this.diemHoa) / 3),2);
    }
}